import numpy as np
import matplotlib.pyplot as plt
#import tool.DSPtools as dsp
from tool._fixedInt import *

## Parametros generales
T     = 1.0/1.0e8 # Periodo de baudio
Nsymb = 1000          # Numero de simbolos
os    = 4
## Parametros de la respuesta en frecuencia
Nfreqs = 256          # Cantidad de frecuencias

## Parametros del filtro de caida cosenoidal
#beta   = [0.0,0.5,1.0] # Roll-Off
Nbauds = 6.0          # Cantidad de baudios del filtro
Nfilt = 11
## Parametros funcionales
Ts = T/os              # Frecuencia de muestreo

def rcosine(beta, Tbaud, oversampling, Nbauds, Norm):
    """ Respuesta al impulso del pulso de caida cosenoidal """
    t_vect = np.arange(-0.5*Nbauds*Tbaud, 0.5*Nbauds*Tbaud, 
                       float(Tbaud)/oversampling)

    y_vect = []
    for t in t_vect:
        y_vect.append(np.sinc(t/Tbaud)*(np.cos(np.pi*beta*t/Tbaud)/
                                        (1-(4.0*beta*beta*t*t/
                                            (Tbaud*Tbaud)))))

    y_vect = np.array(y_vect)

    if(Norm):
        return (t_vect, y_vect/np.sqrt(np.sum(y_vect**2)))
        #return (t_vect, y_vect/y_vect.sum())
    else:
        return (t_vect,y_vect)



(t,filt) = rcosine(1.0, T,os,int(Nbauds),Norm=True)
print filt

qfiltTx = arrayFixedInt(filt, 10, 9,  signedMode='S', roundMode='round', saturateMode='saturate')
print qfiltTx.valueList


fp = open("coeff.v","w")

for ptr in range(len(qfiltTx)):
    fp.write("assign coeff[{}] = 10'd{};\n".format(ptr,qfiltTx[ptr].intvalue))

fp.close() 


