`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/06/2019 02:50:02 PM
// Design Name: 
// Module Name: BER
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BER 
#(
            parameter NB_SR = 1022, NB_COUNTER = 9, NB_ACUM = 64, NB_DIR = 10)

(         
            output[NB_ACUM-1 :0] o_ber,
            output o_led,
            input i_enable,
            input clock,
            input i_data,
            input i_prbs,
            input i_reset,
            input [1:0] i_valid
            );
    
    reg [NB_SR-1 : 0] sr;
    reg [NB_COUNTER-1 : 0] counter;
    reg [NB_ACUM-1 :0]acum;
    reg [NB_DIR-1:0] dir;
    reg [NB_ACUM-1 :0]better_err;
    reg [NB_DIR-1:0] better_dir;
    reg flag;
    //reg data_sample;
    


    
    always@ (posedge clock) begin
        if(i_reset)begin
            sr <= {NB_SR{1'd0}};
            counter <= {NB_COUNTER{1'b0}}; 
            acum <= {NB_ACUM{1'b0}};
            dir <={NB_DIR{1'b0}};
            better_err <={NB_ACUM{1'b1}};
            better_dir <={NB_DIR{1'b0}};
            flag <= 1'b0;
        end
        else if(i_enable)begin
            if(!flag)begin
                
                if(i_valid == 2'd3)begin
                    if (counter<9'd510)begin
                        sr <= {sr[NB_SR-2-:NB_SR-1], i_prbs};
                        better_err <= better_err;
                        better_dir <= better_dir;
                        dir <= dir;
                        //data_sample <= i_data; 
                        if(dir<(NB_SR-1))begin
                            counter <= counter +1;
                            //acum <= acum + (sr[dir] ^ data_sample);
                            acum <= acum + (sr[dir] ^ i_data);
                        end//if(dir<1024)
                    end//if (counter<9'd510))
                
                    else if(counter==9'd510)begin
                        if(acum<better_err)begin
                            better_err<=acum;
                            better_dir<=dir;
                        end//if(acum<better_err)
                        else begin
                            better_err <= better_err;
                            better_dir <= better_dir;
                        end//else
                        counter<={NB_COUNTER{1'b0}};
                        dir <= dir + 1;
                        sr <= {sr[NB_SR-2-:NB_SR-1], i_prbs};
                        acum <= {NB_ACUM{1'b0}}; 
                    end//else if(counter==9'd510)
                end
                
                
                if(dir==(NB_SR-1)) begin
                        flag <= 1'b1;
                        acum <= {NB_ACUM{1'b0}};
                end//if(dir==1024)
            end
            else begin
                if(i_valid == 2'd3)begin
                    acum <= acum + (sr[better_dir] ^ i_data);
                    sr <= {sr[NB_SR-2-:NB_SR-1], i_prbs};
                end
            end
        end
        else begin
            sr <= sr;
            counter <= counter;
            acum <= acum;
            dir <= dir;
            better_err <= better_err;
            better_dir <= better_dir;
            
        end

                                   
    end
    
    assign o_ber = flag? acum : {NB_ACUM{1'b0}};
    assign o_led = flag? 1'b1 : 1'b0;

    
    
    
    
endmodule
