`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/21/2019 06:20:23 PM
// Design Name: 
// Module Name: testbench_ber
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


//`define N_LEDS 4
`define NB_SEL 2
`define NB_COUNT 13
`define NB_SW 4
`define NB_COEFF 10
//`define N_COEFF 24

`timescale 1ns/100ps

module testbench_ber();

   //parameter N_LEDS   = `N_LEDS   ;
   parameter NB_SEL   = `NB_SEL   ;
   parameter NB_COUNT = `NB_COUNT ;
   parameter NB_SW    = `NB_SW    ;
   parameter NB_COEFF = `NB_COEFF ;
   //parameter N_COEFF = `N_COEFF   ;

//   wire [N_LEDS - 1 : 0] o_led    ;
//   wire [N_LEDS - 1 : 0] o_led_b  ;
//   wire [N_LEDS - 1 : 0] o_led_g  ;
//   wire [N_LEDS - 1 : 0] o_led_r  ;
   reg [NB_SW   - 1 : 0] i_sw     ;
   reg                   i_reset  ;
   reg                   clock    ;

   //wire signed [NB_COUNT - 1 : 0] tb_filter_out;
   //wire tb_prbs_out;
   //wire [1:0] tb_valid;
   //wire signed [NB_COEFF-1:0] tb_coeff[N_COEFF-1:0];
   //wire [6-1:0]tb_filter_shiftreg;
   wire prbs_to,delay_to_ber;
   wire [63 : 0]ber;
   wire [1:0]fsm_to;
   wire tb_berflag;
   wire [63 : 0]tb_beracum;
   wire [8 : 0] tb_bercounter;
   wire [9 : 0] tb_berdir;
   wire [1021 : 0] tb_bersr;

   //assign tb_count = tb_shiftleds.u_shiftleds.counter;
//   assign tb_filter_out = testbench.u_top.u_filter.o_data;
//   assign tb_valid = testbench.u_top.u_fsm.o_count;
//   assign tb_prbs_out = testbench.u_top.u_prbs.o_prbs;
//   assign tb_filter_shiftreg = testbench.u_top.u_filter.shiftreg;
    assign tb_berflag = testbench_ber.u_ber.flag;
    assign tb_beracum = testbench_ber.u_ber.acum;
    assign tb_bercounter = testbench_ber.u_ber.counter;
    assign tb_berdir = testbench_ber.u_ber.dir;
    assign tb_bersr = testbench_ber.u_ber.sr;  
    

   initial begin
     i_sw[0]             = 1'b0       ;
      clock               = 1'b0       ;
      i_reset             = 1'b0       ;
      i_sw[2:1]           = `NB_SEL'h0 ;
      i_sw[3]             = 1'b0       ;
      #100 i_reset        = 1'b1       ;
      #100 i_reset        = 1'b0       ;  
      #100 i_sw[0]        = 1'b1       ;
//      #10000 i_sw[1]      = 1'b1       ;
//      #1000000 i_sw[2:1]  = `NB_SEL'h2 ;
//      #1000000 i_sw[3]    = 1'b1       ;
//      #1000000 i_sw[2:1]  = `NB_SEL'h3 ;
      #25000000 $finish                 ;
   end

   always #5 clock = ~clock;

    BER
       u_ber(
             .o_ber(ber),            
             .clock(clock),
             .i_data(delay_to_ber),
             .i_prbs(prbs_to),
             .i_reset(i_reset),
             .i_valid(fsm_to),
             .i_enable(1'b1)
            );
    delay
        u_delay(.clock(clock),
                .i_data(prbs_to),
                .o_data(delay_to_ber));
            
    FSM
        u_fsm(.o_count(fsm_to),
              .i_reset(i_reset),
              .clock(clock));
              
     prbs
        u_prbs(.o_prbs(prbs_to),
               .i_enable(i_sw[0]),
               .i_valid(fsm_to),
               .clock(clock),
               .i_reset(i_reset)
                );



endmodule // tb_shiftledsmodule testbench_ber(


