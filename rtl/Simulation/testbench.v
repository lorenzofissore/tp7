/////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/09/2019 12:22:05 AM
// Design Name: 
// Module Name: testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


// TB
// ShiftLeds
`define N_LEDS 4
`define NB_SEL 2
`define NB_COUNT 13
`define NB_SW 4
`define NB_COEFF 10
`define N_COEFF 24

`timescale 1ns/100ps

module testbench();

   parameter N_LEDS   = `N_LEDS   ;
   parameter NB_SEL   = `NB_SEL   ;
   parameter NB_COUNT = `NB_COUNT ;
   parameter NB_SW    = `NB_SW    ;
  // parameter NB_COEFF = `NB_COEFF ;
  // parameter N_COEFF = `N_COEFF   ;

//   wire [N_LEDS - 1 : 0] o_led    ;
//   wire [N_LEDS - 1 : 0] o_led_b  ;
//   wire [N_LEDS - 1 : 0] o_led_g  ;
//   wire [N_LEDS - 1 : 0] o_led_r  ;
   reg [NB_SW   - 1 : 0] i_sw     ;
   reg                   i_reset  ;
   reg                   clock    ;
   reg [N_LEDS : 0]      i_btn    ;

//   wire [NB_COUNT - 1 : 0] tb_filter_out;
//   wire tb_prbs_out;
//   wire [1:0] tb_valid;
////   wire signed [NB_COEFF-1:0] tb_coeff[N_COEFF-1:0];
//   //wire [6-1:0]tb_filter_shiftreg;
//   wire tb_ber_sr0;
//   wire tb_ber_sr1;   
//   wire tb_ok_ber;
//   wire [63 : 0] tb_ber;
//   wire [63 : 0] tb_better_err;
//   wire [NB_COUNT - 1 : 0] tb_phaser_out;
//   wire [63 : 0] tb_ber_acum;
//   wire [8 : 0]tb_ber_counter;
//   wire tb_ber_data;
   
   

   //assign tb_count = tb_shiftleds.u_shiftleds.counter;
//    assign tb_filter_out = testbench.u_top.u_filter.o_data;
//    assign tb_valid = testbench.u_top.u_fsm.o_count;
//    assign tb_prbs_out = testbench.u_top.u_prbs.o_prbs;
//   //assign tb_filter_shiftreg = testbench.u_top.u_filter.shiftreg;
//    assign tb_ber_sr0 = testbench.u_top.u_ber.sr[0];
//    assign tb_ber_sr1 = testbench.u_top.u_ber.sr[1];
//    assign tb_ber_data = testbench.u_top.u_ber.data_sample;
        
//    assign tb_ok_ber = testbench.u_top.ber_ok;
//    assign tb_ber = testbench.u_top.ber_to;
//    assign tb_better_err = testbench.u_top.u_ber.better_err;
//    assign tb_phaser_out = testbench.u_top.phaser_to_slicer;
//    assign tb_ber_acum = testbench.u_top.u_ber.acum;
//    assign tb_ber_counter = testbench.u_top.u_ber.counter;

   initial begin
      i_sw[0]             = 1'b0       ;
      clock               = 1'b0       ;
      i_reset             = 1'b0       ;
      i_sw[2:1]           = `NB_SEL'h0 ;
      #100 i_reset        = 1'b1       ;
      #100 i_sw[0]        = 1'b1       ;
       i_sw[1]            = 1'b1       ;
       i_sw[2]            = 1'b1       ;
       i_sw[3]            = 1'b0       ;
//      #1000000 i_sw[2:1]  = `NB_SEL'h2 ;
//      #1000000 i_sw[3]    = 1'b1       ;
//      #1000000 i_sw[2:1]  = `NB_SEL'h3 ;
      #25000000 $finish                 ;
   end

   always #5 clock = ~clock;

//shiftleds
//  #(
//    .N_LEDS   (N_LEDS)  ,
//    .NB_SEL   (NB_SEL)  ,
//    .NB_COUNT (NB_COUNT),
//    .NB_SW    (NB_SW)
//    )
//  u_shiftleds
//    (
//     .o_led     (o_led)    ,
//     .o_led_b   (o_led_b)  ,
//     .o_led_g   (o_led_g)  ,
//     .i_sw      (i_sw)     ,
//     .i_reset   (i_reset)  ,
//     .clock     (clock)
//     );
top
   #( 
   )
   u_top
    (
    .o_led_g(o_led_g),
    .clock(clock),
    .i_reset(i_reset),
    .i_sw(i_sw),
    .i_btn(i_btn)
    );

endmodule // tb_shiftleds