`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/25/2019 05:10:13 PM
// Design Name: 
// Module Name: delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module delay(
    input clock,
    input i_data,
    output o_data
    );
    
    reg[8:0]sr;
    
    always@(posedge clock)begin
        sr <= {sr[8-1-:8],i_data};
    end
    
    assign o_data = sr[8];
    
endmodule
