`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/06/2019 02:50:02 PM
// Design Name: 
// Module Name: filter
// Project Name: 
// Target Devices: ^^^^^^^
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module filter
 #(
    parameter N_COEFF = 24, NB_COUNT = 2, NB_DATA = 13, NB_SR = 6, NB_COEFF = 10
    )
  (
    output reg [NB_DATA-1 : 0] o_data,
    input clock,
    input i_reset,
    input i_enable,
    input [1:0]i_valid,
    input i_data
    );
    
        
   `include "/home/f/Documents/Funda/Verilog/Coeff_generator/coeff.v"
    
    wire signed [NB_COEFF-1:0] coeff[N_COEFF-1:0];
    reg[NB_SR-1:0]shiftreg;
    reg data_d;
    wire [NB_DATA-1 : 0] data_w;
    
    always@ (posedge clock)begin
        if(i_reset)begin
            o_data<={NB_DATA{1'b0}};
        end 
        else if(i_enable)begin
            o_data<=data_w;
        end
        else begin
            o_data<=data_w;
        end
    end
    
    always@(posedge clock)begin
        if(i_enable && i_valid==2'b11)begin
            shiftreg<={shiftreg[NB_SR-2-:NB_SR-1],i_data};
        end
        else
            shiftreg<=shiftreg;
    end

    
    assign data_w = ((shiftreg[0])? - coeff[i_valid+0]:coeff[i_valid+0])+
                    ((shiftreg[1])? - coeff[i_valid+4]:coeff[i_valid+4])+
                    ((shiftreg[2])? - coeff[i_valid+8]:coeff[i_valid+8])+
                    ((shiftreg[3])? - coeff[i_valid+12]:coeff[i_valid+12])+
                    ((shiftreg[4])? - coeff[i_valid+16]:coeff[i_valid+16])+
                    ((shiftreg[5])? - coeff[i_valid+20]:coeff[i_valid+20]);
    
endmodule
