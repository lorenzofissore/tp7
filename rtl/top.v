`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/06/2019 02:50:02 PM
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top

    (
//    output [3:0]o_led_b,
    output [3:0]o_led_g,
//    output [3:0]o_led_r,
//    output [3:0] o_led,
    input clock,
    input i_reset,
    input [3:0]i_sw,
    input [3:0]i_btn
    );
    wire prbs_to_filter;
    wire slicer_to_ber;
    wire [12:0]filter_to;
    wire [12:0]phaser_to_slicer;
    wire [1:0]valid_w;
    wire reset;
    wire [63 : 0] ber_to;
    wire ber_reset;
    wire ber_ok;
    assign reset =  ~i_reset;
    
    assign ber_reset = reset | i_btn[0];
       
    filter
        u_filter(.o_data(filter_to),
                 .i_reset(reset),
                 .i_enable(i_sw[1]),
                 .i_valid(valid_w),
                 .clock(clock),
                 .i_data(prbs_to_filter));
    prbs
        u_prbs(.o_prbs(prbs_to_filter),
               .i_enable(i_sw[0]),
               .i_valid(valid_w),
               .clock(clock),
               .i_reset(reset)
                );
    FSM
        u_fsm(.o_count(valid_w),
              .i_reset(reset),
              .clock(clock));
//    ila_wrapper
//        u_ila(.clk_0(clock),
//              .probe0_0(filter_to_slicer));
              
    slicer
        u_slicer(
                 .i_data(phaser_to_slicer),
                 .o_data(slicer_to_ber));
                 
    
    BER
       u_ber(
             .o_led(ber_ok),
             .o_ber(ber_to),            
             .clock(clock),
             .i_data(slicer_to_ber),
             .i_prbs(prbs_to_filter),
             .i_reset(ber_reset),
             .i_valid(valid_w),
             .i_enable(1'b1)
            );
            
      ila ila_i
           (.clk_0(clock),
            .probe0_0(filter_to),
            .probe1_0(ber_to));
      phaser
        u_phaser    
            (.o_data(phaser_to_slicer),
             .i_data(filter_to),
             .clock(clock),
             .i_reset(reset),
             .phase(i_sw[3:2]));
             
   assign ber_ok = o_led_g[0];


    
endmodule
