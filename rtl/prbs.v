`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/06/2019 02:50:02 PM
// Design Name: 
// Module Name: prbs
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module prbs #(parameter SEED = 9'h1AA, NB_SR = 9, LOW = 4, HIGH=8)
(
    output o_prbs,
    input clock,
    input i_enable,
    input [1:0]i_valid,
    input i_reset
    );

reg [NB_SR:0] shiftreg;

always @(posedge clock)begin
    if(i_reset)begin
        shiftreg<= SEED;
    end
    else if(i_enable && i_valid==2'd3)begin
        shiftreg<={shiftreg[(NB_SR-1)-1-:NB_SR-1],shiftreg[LOW]^shiftreg[HIGH]};
    end
    else begin
        shiftreg<=shiftreg;
        end
end
assign o_prbs = shiftreg[NB_SR-1];
endmodule
