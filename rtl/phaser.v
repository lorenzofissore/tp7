`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/07/2019 03:42:29 PM
// Design Name: 
// Module Name: phaser
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module phaser#(parameter N_SR = 4, NB_SR =13  )
    (
    output [12:0] o_data,
    input [12:0] i_data,
    input clock,
    input i_reset,
    input [1:0] phase
    );
    
    reg [(N_SR*NB_SR)-1:0] data_sr; 
    always@(posedge clock)begin
        if(i_reset)begin
            data_sr<=52'b0;
        end
   
        data_sr <= {data_sr[((N_SR-1)*NB_SR)-1 -:((N_SR-1)*NB_SR)],i_data};   
    end
    
    assign o_data = data_sr[phase*NB_SR +:NB_SR];
    
endmodule
