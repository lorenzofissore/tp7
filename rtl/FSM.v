`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/07/2019 12:07:38 PM
// Design Name: 
// Module Name: FSM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FSM
    #(parameter NB_COUNT = 2
    )
    (
    output reg [1:0]o_count,
    input clock,
    input i_reset
    );
    
    always@ (posedge clock)begin
        if(i_reset)
            o_count<={NB_COUNT{1'b0}};
        else
            o_count<=o_count+1;
    end
endmodule
